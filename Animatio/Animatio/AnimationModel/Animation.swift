//
//  Animation.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/17/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public class Animation: NSObject, CAAnimationDelegate {
	public let builder: AnimationBuilder
	public let layer: CALayer
	public var key: String?

	public var onAnimationStop: ((_ anim: CAAnimation, _ finished: Bool) -> Void)?
	public var onAnimationStart: ((_ anim: CAAnimation) -> Void)?

	public init(builder: AnimationBuilder, layer: CALayer, key: String? = nil) {
		self.builder = builder
		self.layer = layer
		self.key = key

		super.init()
	}

	public func run() {
		layer.add(builder.build(), forKey: key)
	}

	public func remove() {
		guard let key = key else { return }
		layer.removeAnimation(forKey: key)
	}

	public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
		onAnimationStop?(anim, flag)
	}

	public func animationDidStart(_ anim: CAAnimation) {
		onAnimationStart?(anim)
	}
}
