//
//  FillMode.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/17/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public enum FillMode {
	case forwards
	case backwards
	case both
	case removed

	var mode: String {
		switch self {
		case .forwards:
			return kCAFillModeForwards
		case .backwards:
			return kCAFillModeBackwards
		case .both:
			return kCAFillModeBoth
		case .removed:
			return kCAFillModeRemoved
		}
	}
}
