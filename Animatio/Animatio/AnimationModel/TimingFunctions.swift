//
//  TimingFunctions.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/17/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public enum TimingFunction {
	case easeIn
	case easeOut
	case easeInOut
	case linear
	case custom(c1x: Float, c1y: Float, c2x: Float, c2y: Float)
	case `default`

	public var function: CAMediaTimingFunction {
		switch self {
		case .easeIn:
			return CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
		case .easeOut:
			return CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
		case .easeInOut:
			return CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
		case .linear:
			return CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
		case .custom(let c1x, let c1y, let c2x, let c2y):
			return CAMediaTimingFunction(controlPoints: c1x, c1y, c2x, c2y)
		default:
			return CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
		}
	}
}
