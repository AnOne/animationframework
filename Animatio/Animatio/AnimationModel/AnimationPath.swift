//
//  Animations.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/17/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public enum AnimationPath: String {
	case opacity = "opacity"
	case backgroundColor = "backgroundColor"

	case position = "position"
	case positionX = "position.x"
	case positionY = "position.y"

	case shadowOffset = "shadowOffset"
	case shadowOpacity = "shadowOpacity"

	case scale = "transform.scale"
	case rotation = "transform.rotation"

	case width = "bounds.size.width"

	case strokeEnd = "strokeEnd"
	case strokeStart = "strokeStart"

	case path = "path"

	// FIXME: Used in AnimatorExample
	public var basic: CABasicAnimation {
		return CABasicAnimation(keyPath: self)
	}

	public var keyFrame: CAKeyframeAnimation {
		return CAKeyframeAnimation(keyPath: self)
	}
}

// FIXME: Unused
public enum AnimationType {
	case basic
	case keyframe
}
