//
//  CAAnimation+Extensions.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/18/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

extension CABasicAnimation {
	convenience init(keyPath: AnimationPath) {
		self.init(keyPath: keyPath.rawValue)
	}
}

extension CAKeyframeAnimation {
	convenience init(keyPath: AnimationPath) {
		self.init(keyPath: keyPath.rawValue)
	}
}
