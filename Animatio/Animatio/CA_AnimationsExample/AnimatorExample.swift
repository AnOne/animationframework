//
//  Animator.swift
//  AnimationFramework
//
//  Created by Oleksii Zablotskyi on 6/16/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public class AnimatorExample {

	public init() {}

	public func strokeRotationAnimation(for shapeLayer: CAShapeLayer) {
		let startAnimation = AnimationPath.strokeStart.basic
		let endAnimation = AnimationPath.strokeEnd.basic

		startAnimation.fromValue = -0.75
		startAnimation.toValue = 1.0

		endAnimation.fromValue = 0
		endAnimation.toValue = 1.0

		let group = CAAnimationGroup()
		group.animations = [startAnimation, endAnimation]
		group.duration = 1.7
		group.repeatCount = .infinity
		group.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)

		let rotation = AnimationPath.rotation.basic
		rotation.fromValue = 0
		rotation.toValue = 2 * CGFloat.pi
		rotation.duration = 6
		rotation.repeatCount = .infinity

		shapeLayer.add(group, forKey: nil)
		shapeLayer.add(rotation, forKey: nil)
	}

	public func warmHoleAnimation(in view: UIView) {
		let circleFillColor = UIColor(red: 126/255.0, green: 137/255.0, blue: 232/255.0, alpha: 1.0)
		let darkColor = UIColor(red: 77/255.0, green: 85/255.0, blue: 143/255.0, alpha: 1.0)

		let diameter: CGFloat = 25
		let distance: CGFloat = 70
		let pointsCount = 4
		let scaleDuration = 1.5
		let positionChangeDuration = scaleDuration*CFTimeInterval(pointsCount-1)

		let shapeLayer = CAShapeLayer()
		let lineLayer = CAShapeLayer()

		let width: CGFloat = CGFloat(pointsCount-1)*distance + diameter
		let circlesPath = UIBezierPath()

		var positions = [NSNumber]()
		let size = CGSize(width: diameter, height: diameter)
		for i in 0 ..< pointsCount {
			let x = i * Int(distance)

			let origin = CGPoint(x: x, y: 0)
			let rect = CGRect(origin: origin, size: size)
			let circle = UIBezierPath(ovalIn: rect)
			circlesPath.append(circle)

			positions.append(NSNumber(value: Float(i)*Float(distance)))
		}

		shapeLayer.path = circlesPath.cgPath
		shapeLayer.fillColor = circleFillColor.cgColor
		shapeLayer.frame = CGRect(x: view.bounds.midX - width/2,
								  y: view.frame.midY - diameter/2,
								  width: width,
								  height: diameter)

		lineLayer.frame = CGRect(x: -diameter/2, y: 0, width: diameter, height: diameter)
		lineLayer.anchorPoint = CGPoint(x: 0, y: 0.5)
		lineLayer.fillColor = UIColor.clear.cgColor
		lineLayer.backgroundColor = darkColor.cgColor
		lineLayer.cornerRadius = diameter/2

		shapeLayer.addSublayer(lineLayer)
		view.layer.addSublayer(shapeLayer)

		let positionAnimation = AnimationPath.positionX.keyFrame
		positionAnimation.values = positions
		positionAnimation.duration = positionChangeDuration
		positionAnimation.autoreverses = true
		positionAnimation.repeatCount = .infinity
		positionAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)

		let widthAnimation = AnimationPath.width.keyFrame
		let values = [diameter, min(diameter*3, distance-10), diameter]
			.map { Int.init($0) }
			.map(NSNumber.init(value:))
		widthAnimation.values = values

		widthAnimation.keyTimes = [0.0, 0.5, 1.0]
		widthAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)

		widthAnimation.repeatCount = .infinity
		widthAnimation.duration = scaleDuration

		lineLayer.add(positionAnimation, forKey: nil)
		lineLayer.add(widthAnimation, forKey: nil)
	}

	public func checkMarkAnimation(in view: UIView) {
		let darkColor = UIColor(red: 77/255.0, green: 85/255.0, blue: 143/255.0, alpha: 1.0)

		let diameter: CGFloat = 100
		let origin = CGPoint(x: view.bounds.midX - 50, y: view.bounds.midY - 50)
		let size = CGSize(width: diameter, height: diameter)
		let rect = CGRect(origin: origin, size: size)

		let lineWidth: CGFloat = 6

		let circlePath = UIBezierPath(arcCenter: CGPoint(x: diameter/2, y: diameter/2),
									  radius: diameter/2,
									  startAngle: 0,
									  endAngle: .pi,
									  clockwise: false)

		let checkPath = UIBezierPath()
		checkPath.move(to: CGPoint(x: 0, y: diameter/2))
		checkPath.addLine(to: CGPoint(x: diameter/2, y: diameter))
		checkPath.addLine(to: CGPoint(x: diameter-20, y: 10))

		circlePath.append(checkPath)

		let circleLayer = CAShapeLayer()
		circleLayer.fillColor = nil
		circleLayer.strokeColor = darkColor.cgColor
		circleLayer.lineWidth = lineWidth
		circleLayer.lineCap = kCALineCapRound
		circleLayer.lineJoin = kCALineJoinRound
		circleLayer.frame = rect

		circleLayer.path = circlePath.cgPath

		view.layer.addSublayer(circleLayer)

		let strokeDuration: CFTimeInterval = 0.5

		let rotationDuration: CFTimeInterval = 0.75
		let rotationCount: Float = 7
		let rotationOffset = strokeDuration

		let pathDuration = 0.75
		let pathOffset = CFTimeInterval(rotationCount) * rotationDuration + rotationOffset

		// MARK: - Stroke Animation
		let strokeEndAnim = AnimationPath.strokeEnd.basic
		let strokeEnd: CGFloat = 0.48
		strokeEndAnim.fromValue = 0
		strokeEndAnim.toValue = strokeEnd
		strokeEndAnim.duration = strokeDuration

		circleLayer.strokeEnd = strokeEnd
		circleLayer.add(strokeEndAnim, forKey: "StrokeEndAnimation")

		// MARK: - Rotation Animation
		let rotationAnimation = AnimationPath.rotation.basic
		rotationAnimation.fromValue = 0
		rotationAnimation.byValue = -2 * CGFloat.pi

		rotationAnimation.duration = rotationDuration
		rotationAnimation.repeatCount = rotationCount
		rotationAnimation.beginTime = CACurrentMediaTime() + rotationOffset

		circleLayer.add(rotationAnimation, forKey: "RotationAnimation")

		// MARK: - Path animation
		let strokeStart = AnimationPath.strokeStart.basic
		strokeStart.duration = pathDuration
		strokeStart.beginTime = CACurrentMediaTime() + pathOffset
		strokeStart.fromValue = 0.0
		strokeStart.toValue = strokeEnd + 0.05

		strokeEndAnim.fromValue = strokeEnd
		strokeEndAnim.toValue = 1.0
		strokeEndAnim.duration = pathDuration
		strokeEndAnim.beginTime = CACurrentMediaTime() + pathOffset

		strokeEndAnim.isRemovedOnCompletion = false
		strokeEndAnim.fillMode = kCAFillModeForwards
		circleLayer.add(strokeEndAnim, forKey: "PathEndAnimation")

		strokeStart.isRemovedOnCompletion = false
		strokeStart.fillMode = kCAFillModeForwards
		circleLayer.add(strokeStart, forKey: "PathStartAnimation")
	}
}
