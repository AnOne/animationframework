//
//  GroupAnimationsBuilder.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/18/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public class GroupAnimationsBuilder: AnimationBuilder {

	private(set) var builders: [AnimationBuilder] = []

	public override init() {}

	public func builders(_ builders: [AnimationBuilder]) -> GroupAnimationsBuilder {
		self.builders = builders
		return self
	}

	override func build() -> CAAnimation {
		let group = CAAnimationGroup()
		group.duration = duration
		group.beginTime = beginTime

		group.repeatCount = repeatCount

		group.timingFunction = timingFunction
		group.fillMode = fillMode
		group.isRemovedOnCompletion = isRemovedOnCompletion
		group.autoreverses = autoreverses

		group.animations = builders.map { $0.build() }

		return group
	}
	/*
	let basic = CABasicAnimation(keyPath: keyPath)
	basic.fromValue = fromValue
	basic.toValue = toValue

	basic.duration = duration
	basic.beginTime = beginTime

	basic.repeatCount = repeatCount

	basic.timingFunction = timingFunction
	basic.fillMode = fillMode
	basic.isRemovedOnCompletion = isRemovedOnCompletion
	basic.autoreverses = autoreverses

	return basic
	*/
}
