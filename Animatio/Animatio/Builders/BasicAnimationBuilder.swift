//
//  BasicAnimationBuilder.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/18/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public class BasicAnimationBuilder<T>: AnimationBuilder {
	private let keyPath: String

	private var fromValue: T?
	private var toValue: T?

	public init(path: AnimationPath) {
		self.keyPath = path.rawValue
	}

	public func fromValue(_ fromValue: T?) -> BasicAnimationBuilder {
		self.fromValue = fromValue
		return self
	}

	public func toValue(_ toValue: T?) -> BasicAnimationBuilder {
		self.toValue = toValue
		return self
	}

	override func build() -> CABasicAnimation {
		let basic = CABasicAnimation(keyPath: keyPath)
		basic.fromValue = fromValue
		basic.toValue = toValue

		basic.duration = duration
		basic.beginTime = beginTime

		basic.repeatCount = repeatCount

		basic.timingFunction = timingFunction
		basic.fillMode = fillMode
		basic.isRemovedOnCompletion = isRemovedOnCompletion
		basic.autoreverses = autoreverses

		return basic
	}
}
