//
//  AnimationBuilder.swift
//  Animatio
//
//  Created by Oleksii Zablotskyi on 6/18/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

public class AnimationBuilder {
	private(set) var duration: CFTimeInterval = 0
	private(set) var beginTime: CFTimeInterval = 0

	private(set) var repeatCount: Float = 0

	private(set) var timingFunction: CAMediaTimingFunction?

	private(set) var fillMode: String = kCAFillModeRemoved

	private(set) var isRemovedOnCompletion: Bool = true
	private(set) var autoreverses: Bool = false

	public func duration(_ duration: CFTimeInterval) -> AnimationBuilder {
		self.duration = duration
		return self
	}

	public func beginTime(offset: CFTimeInterval) -> AnimationBuilder {
		self.beginTime = CACurrentMediaTime() + offset
		return self
	}

	public func repeatCount(_ repeatCount: Float) -> AnimationBuilder {
		self.repeatCount = repeatCount
		return self
	}

	public func timingFunction(_ timingFunction: TimingFunction) -> AnimationBuilder {
		self.timingFunction = timingFunction.function
		return self
	}

	public func fillMode(_ fillMode: FillMode) -> AnimationBuilder {
		self.fillMode = fillMode.mode
		return self
	}

	public func removedOnCompletion(_ isRemovedOnCompletion: Bool) -> AnimationBuilder {
		self.isRemovedOnCompletion = isRemovedOnCompletion
		return self
	}

	public func autoreverses(_ autoreverses: Bool) -> AnimationBuilder {
		self.autoreverses = autoreverses
		return self
	}

	func build() -> CAAnimation {
		fatalError("Should be implemented in subclasses")
	}
}
