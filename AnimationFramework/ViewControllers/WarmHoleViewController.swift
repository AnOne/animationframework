//
//  WarmHoleViewController.swift
//  AnimationFramework
//
//  Created by Oleksii Zablotskyi on 6/17/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

import UIKit
import Animatio

final class WarmHoleViewController: UIViewController {

	let animator = AnimatorExample()

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		animator.warmHoleAnimation(in: view)
	}
}
