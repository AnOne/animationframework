//
//  ViewController.swift
//  AnimationFramework
//
//  Created by Oleksii Zablotskyi on 6/16/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

import UIKit
import Animatio

final class StrokeRotationViewController: UIViewController {

	let animator = Animator()

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		let shapeLayer = buildShapeLayer()
		view.layer.addSublayer(shapeLayer)

		animator.strokeRotationAnimation(for: shapeLayer)
	}

	private func buildShapeLayer() -> CAShapeLayer {
		let origin = CGPoint(x: view.bounds.midX - 50, y: view.bounds.midY - 50)
		let size = CGSize(width: 100, height: 100)
		let rect = CGRect(origin: origin, size: size)
		let ovalRect = CGRect(origin: .zero, size: size)
		let circle = UIBezierPath(ovalIn: ovalRect)

		let shapeLayer = CAShapeLayer()
		shapeLayer.fillColor = UIColor.yellow.cgColor
		shapeLayer.strokeColor = UIColor.red.cgColor
		shapeLayer.lineWidth = 4
		shapeLayer.lineCap = kCALineCapRound
		shapeLayer.path = circle.cgPath
		shapeLayer.frame = rect

		return shapeLayer
	}
}
