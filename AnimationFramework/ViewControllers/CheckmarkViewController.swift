//
//  CheckmarkViewController.swift
//  AnimationFramework
//
//  Created by Oleksii Zablotskyi on 6/17/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

import UIKit
import Animatio

final class CheckmarkViewController: UIViewController {

	let animator = Animator()

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

		let circleLayer = buildCircleLayer()
		view.layer.addSublayer(circleLayer)

		let timer = Timer.scheduledTimer(withTimeInterval: 8, repeats: true) { [weak self] _ in
			self?.animator.checkMarkAnimation(for: circleLayer)
		}

		timer.fire()
	}

	fileprivate func buildCircleLayer() -> CAShapeLayer {
		let darkColor = UIColor(red: 77/255.0, green: 85/255.0, blue: 143/255.0, alpha: 1.0)

		let diameter: CGFloat = 100
		let origin = CGPoint(x: view.bounds.midX - 50, y: view.bounds.midY - 50)
		let size = CGSize(width: diameter, height: diameter)
		let rect = CGRect(origin: origin, size: size)

		let lineWidth: CGFloat = 6

		let circlePath = UIBezierPath(arcCenter: CGPoint(x: diameter/2, y: diameter/2),
									  radius: diameter/2,
									  startAngle: 0,
									  endAngle: .pi,
									  clockwise: false)

		let checkPath = UIBezierPath()
		checkPath.move(to: CGPoint(x: 0, y: diameter/2))
		checkPath.addLine(to: CGPoint(x: diameter/2, y: diameter))
		checkPath.addLine(to: CGPoint(x: diameter-20, y: 10))

		circlePath.append(checkPath)

		let circleLayer = CAShapeLayer()
		circleLayer.fillColor = nil
		circleLayer.strokeColor = darkColor.cgColor
		circleLayer.lineWidth = lineWidth
		circleLayer.lineCap = kCALineCapRound
		circleLayer.lineJoin = kCALineJoinRound
		circleLayer.frame = rect

		circleLayer.path = circlePath.cgPath

		return circleLayer
	}

	deinit {
		print("DEINITED: - CheckmarkViewController")
	}
}
