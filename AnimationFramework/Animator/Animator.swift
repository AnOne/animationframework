//
//  Animator.swift
//  AnimationFramework
//
//  Created by Oleksii Zablotskyi on 6/16/18.
//  Copyright © 2018 BadBit. All rights reserved.
//

import UIKit
import Animatio

final class Animator {

	init() {}

	func strokeRotationAnimation(for shapeLayer: CAShapeLayer) {
		let strokeStartBuilder = BasicAnimationBuilder<CGFloat>(path: .strokeStart)
			.fromValue(-0.75)
			.toValue(1.0)

		let strokeEndBuilder = BasicAnimationBuilder<CGFloat>(path: .strokeEnd)
			.fromValue(0)
			.toValue(1.0)

		let strokeGroup = GroupAnimationsBuilder()
			.builders([strokeStartBuilder, strokeEndBuilder])
			.duration(1.7)
			.repeatCount(.infinity)
			.timingFunction(.easeOut)

		let rotationBuilder = BasicAnimationBuilder<CGFloat>(path: .rotation)
			.fromValue(0)
			.toValue(2 * .pi)
			.duration(6)
			.repeatCount(.infinity)

		let stroke = Animation(builder: strokeGroup, layer: shapeLayer)
		let rotation = Animation(builder: rotationBuilder, layer: shapeLayer)

		stroke.run()
		rotation.run()
/*
		let startAnimation = AnimationPath.strokeStart.basic
		let endAnimation = AnimationPath.strokeEnd.basic

		startAnimation.fromValue = -0.75
		startAnimation.toValue = 1.0

		endAnimation.fromValue = 0
		endAnimation.toValue = 1.0

		let group = CAAnimationGroup()
		group.animations = [startAnimation, endAnimation]
		group.duration = 1.7
		group.repeatCount = .infinity
		group.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)

		let rotation = AnimationPath.rotation.basic
		rotation.fromValue = 0
		rotation.toValue = 2 * CGFloat.pi
		rotation.duration = 6
		rotation.repeatCount = .infinity

		shapeLayer.add(group, forKey: nil)
		shapeLayer.add(rotation, forKey: nil)
*/
	}

	public func checkMarkAnimation(for circleLayer: CAShapeLayer) {
		let checkmarkKey = "CheckmarkAnimation"

		circleLayer.removeAnimation(forKey: checkmarkKey)

		// Duration, Offset
		let arcDuration: CFTimeInterval = 0.5

		let rotationDuration: CFTimeInterval = 0.75
		let rotationCount: Float = 7
		let rotationOffset = arcDuration

		let checkmarkDuration = 0.75
		let checkmarkOffset = CFTimeInterval(rotationCount) * rotationDuration + rotationOffset

		// arc-checkmark border value
		let strokeEnd: CGFloat = 0.48

		// MARK: - Arc Animation
		let arcAnimationBuilder = BasicAnimationBuilder<CGFloat>(path: .strokeEnd)
			.fromValue(0)
			.toValue(strokeEnd)
			.duration(arcDuration)

		circleLayer.strokeEnd = strokeEnd

		// MARK: - Rotation Animation
		let rotationAnimationBuilder = BasicAnimationBuilder<CGFloat>(path: .rotation)
			.fromValue(0)
			.toValue(-2 * .pi)
			.duration(rotationDuration)
			.repeatCount(rotationCount)
			.beginTime(offset: rotationOffset)

		// MARK: - Checkmark animation
		let checkmarkStartAnimationBuilder = BasicAnimationBuilder<CGFloat>(path: .strokeStart)
			.fromValue(0.0)
			.toValue(strokeEnd + 0.05)

		let checkmarkEndAnimationBuilder = BasicAnimationBuilder<CGFloat>(path: .strokeEnd)
			.fromValue(strokeEnd)
			.toValue(1.0)

		let checkmarkGroup = GroupAnimationsBuilder()
			.builders([checkmarkStartAnimationBuilder, checkmarkEndAnimationBuilder])
			.duration(checkmarkDuration)
			.beginTime(offset: checkmarkOffset)
			.removedOnCompletion(false)
			.timingFunction(.easeOut)
			.fillMode(.forwards)

		let arcAnimation = Animation(builder: arcAnimationBuilder, layer: circleLayer)
		let rotationAnimation = Animation(builder: rotationAnimationBuilder, layer: circleLayer)
		let checkmarkAnimation = Animation(builder: checkmarkGroup, layer: circleLayer, key: checkmarkKey)

		arcAnimation.run()
		rotationAnimation.run()
		checkmarkAnimation.run()

		/*
		// MARK: - Arc Animation
		let strokeEndAnim = AnimationPath.strokeEnd.basic
		let strokeEnd: CGFloat = 0.48
		strokeEndAnim.fromValue = 0
		strokeEndAnim.toValue = strokeEnd
		strokeEndAnim.duration = arcDuration

		circleLayer.strokeEnd = strokeEnd
		circleLayer.add(strokeEndAnim, forKey: "StrokeEndAnimation")

		// MARK: - Rotation Animation
		let rotationAnimation = AnimationPath.rotation.basic
		rotationAnimation.fromValue = 0
		rotationAnimation.byValue = -2 * CGFloat.pi

		rotationAnimation.duration = rotationDuration
		rotationAnimation.repeatCount = rotationCount
		rotationAnimation.beginTime = CACurrentMediaTime() + rotationOffset

		circleLayer.add(rotationAnimation, forKey: "RotationAnimation")

		// MARK: - Checkmark animation
		let strokeStart = AnimationPath.strokeStart.basic
		strokeStart.duration = checkmarkDuration
		strokeStart.beginTime = CACurrentMediaTime() + checkmarkOffset
		strokeStart.fromValue = 0.0
		strokeStart.toValue = strokeEnd + 0.05

		strokeEndAnim.fromValue = strokeEnd
		strokeEndAnim.toValue = 1.0
		strokeEndAnim.duration = checkmarkDuration
		strokeEndAnim.beginTime = CACurrentMediaTime() + checkmarkOffset

		strokeEndAnim.isRemovedOnCompletion = false
		strokeEndAnim.fillMode = kCAFillModeForwards
		circleLayer.add(strokeEndAnim, forKey: "PathEndAnimation")

		strokeStart.isRemovedOnCompletion = false
		strokeStart.fillMode = kCAFillModeForwards
		circleLayer.add(strokeStart, forKey: "PathStartAnimation")
		*/
	}
}

/*
public func warmHoleAnimation(in view: UIView) {
let circleFillColor = UIColor(red: 126/255.0, green: 137/255.0, blue: 232/255.0, alpha: 1.0)
let darkColor = UIColor(red: 77/255.0, green: 85/255.0, blue: 143/255.0, alpha: 1.0)

let diameter: CGFloat = 25
let distance: CGFloat = 70
let pointsCount = 4
let scaleDuration = 1.5
let positionChangeDuration = scaleDuration*CFTimeInterval(pointsCount-1)

let shapeLayer = CAShapeLayer()
let lineLayer = CAShapeLayer()

let width: CGFloat = CGFloat(pointsCount-1)*distance + diameter
let circlesPath = UIBezierPath()

var positions = [NSNumber]()
let size = CGSize(width: diameter, height: diameter)
for i in 0 ..< pointsCount {
let x = i * Int(distance)

let origin = CGPoint(x: x, y: 0)
let rect = CGRect(origin: origin, size: size)
let circle = UIBezierPath(ovalIn: rect)
circlesPath.append(circle)

positions.append(NSNumber(value: Float(i)*Float(distance)))
}

shapeLayer.path = circlesPath.cgPath
shapeLayer.fillColor = circleFillColor.cgColor
shapeLayer.frame = CGRect(x: view.bounds.midX - width/2,
y: view.frame.midY - diameter/2,
width: width,
height: diameter)

lineLayer.frame = CGRect(x: -diameter/2, y: 0, width: diameter, height: diameter)
lineLayer.anchorPoint = CGPoint(x: 0, y: 0.5)
lineLayer.fillColor = UIColor.clear.cgColor
lineLayer.backgroundColor = darkColor.cgColor
lineLayer.cornerRadius = diameter/2

shapeLayer.addSublayer(lineLayer)
view.layer.addSublayer(shapeLayer)

let positionAnimation = AnimationPath.positionX.keyFrame
positionAnimation.values = positions
positionAnimation.duration = positionChangeDuration
positionAnimation.autoreverses = true
positionAnimation.repeatCount = .infinity
positionAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)

let widthAnimation = AnimationPath.width.keyFrame
let values = [diameter, min(diameter*3, distance-10), diameter]
.map { Int.init($0) }
.map(NSNumber.init(value:))
widthAnimation.values = values

widthAnimation.keyTimes = [0.0, 0.5, 1.0]
widthAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)

widthAnimation.repeatCount = .infinity
widthAnimation.duration = scaleDuration

lineLayer.add(positionAnimation, forKey: nil)
lineLayer.add(widthAnimation, forKey: nil)
}
}
*/
